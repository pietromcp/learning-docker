﻿using System;
using System.Collections.Generic;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace HogwartsBot {
    class Program {
        static ITelegramBotClient botClient;

        static void Main() {
            var token = Environment.GetEnvironmentVariable("TELEGRAM_BOT_TOKEN");
            Console.WriteLine($"Connecting to Telegram... ||{token.Substring(0,5)}||");
            botClient = new TelegramBotClient(token);
            try {
                var me = botClient.GetMeAsync().Result;
                Console.WriteLine(
                    $"Hello, World! I am user {me.Id} and my name is {me.FirstName}."
                );

                botClient.OnMessage += Bot_OnMessage;
                botClient.StartReceiving();
                //Thread.Sleep(int.MaxValue);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message, e.StackTrace);
            }
        }

        static async void Bot_OnMessage(object sender, MessageEventArgs e) {
            try {
                if (e.Message.Text != null)
                {
                    Console.WriteLine($"Received a text message in chat {e.Message.Chat.Id}.");

                    await botClient.SendTextMessageAsync(
                        chatId: e.Message.Chat,
                        text:   CreateResponse(e.Message)
                    );

                    if (Characters.ContainsKey(e.Message.Text)) {
                        Message message = await botClient.SendPhotoAsync(
                            chatId: e.Message.Chat,
                            photo: $"https://gitlab.com/pietrom/hello-hogwarts-chatbot-resources/-/raw/master/{e.Message.Text}.jpg",
                            caption: $"<b>{e.Message.Text} {Characters[e.Message.Text]}</b>.",
                            parseMode: ParseMode.Html
                        );
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message, ex);
            }
        }

        private static string CreateResponse(Message msg) {
            if (Characters.ContainsKey(msg.Text)) {
                return $"Hello, {msg.Text} {Characters[msg.Text]}";
            }

            return $"Hello, {msg.Text}! Known characters are {string.Join(',', Characters.Keys)}";
        }

        static readonly IDictionary<string, string> Characters = new Dictionary<string, string> {
            {"Harry", "Potter"},
            {"Hermione", "Granger"},
            {"Ron", "Weasley"},
            {"Albus", "Silente"},
            {"Severus", "Piton"},
            {"Draco", "Malfoi"},
            {"Neville", "Paciock"},
            {"Luna", "Lovegood"},
            {"Jini", "Weasley"},
            {"Lucius", "Malfoi"},
            {"Tom", "Riddle"},
            {"Bellatrix", "Lestrange"},
            {"Voldemort", ""}
        };
    }
}