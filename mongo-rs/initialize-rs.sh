#!/bin/bash
file_env 'MONGO_REPLICA_SET'
#file_env 'MONGO_REPLICA_SET'
if [ "${MONGO_REPLICA_SET}" != "" ] ; then
	echo "####### Initialize RS ${MONGO_REPLICA_SET} on ${HOSTNAME}"
	command="rs.initiate({_id: \"${MONGO_REPLICA_SET}\", members: [{_id: 0, host: \"${HOSTNAME}:27017\"}]})"
	echo ${command}
	mongo --eval "rs.initiate({_id: \"testing-rs\", members:[{_id: 0, host:\"${HOSTNAME}:27017\"}]})"
fi