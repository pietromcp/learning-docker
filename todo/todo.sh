# Setup fileserver
sudo apt update
sudo apt install nfs-kernel-server
sudo mkdir -p /tank/honey-files
sudo chown nobody:nogroup /tank/honey-files
sudo chmod 777 /tank/honey-files
sudo bash -c "echo '/tank/honey-files 172.29.6.0/16(rw,sync,no_subtree_check,no_root_squash)' > /etc/exports"
sudo bash -c "echo '/tank/honey-files 172.29.6.0/16(rw,sync,no_subtree_check,no_root_squash,anonuid=0,anongid=0,no_acl)' >> /etc/exports"

sudo exportfs -a
sudo systemctl restart nfs-kernel-server


docker volume create --driver local \
  --opt type=nfs \
  --opt o=addr=172.29.6.200,uid=1000,gid=1000,rw \
  --opt device=:/tank/honey-files \
  honey-files

docker service create --name nginx1 \
  --mount "type=volume,source=honey-files,destination=/usr/share/nginx/html,readonly=false" \
  --replicas 1 -p 30001:80  nginx:latest


docker service create --name nginx1 -9 30001:80 --mount 'type=volume,source=honey-files,target=/usr/share/nginx/html,volume-driver=local,volume-opt=type=nfs,volume-opt=device=:/,"volume-opt=o=172.29.6.200,rw,nfsvers=4,async"' nginx:latest




sudo mount -t nfs 172.29.6.200:/tank/honey-files shared/
docker volume create honey-files --driver local --opt type=nfs --opt o=addr=172.29.6.200 --opt device=:/tank/honey-files

docker run -it -v honey-files:/honey debian bash

docker run --name nginx1 --rm -v honey-files:/usr/share/nginx/html -p 30001:80 nginx:latest
docker service create --name nginx1 -p 30002:80 --mount 'src=honey-files,target=/usr/share/nginx/html' nginx:latest





docker plugin install jmaitrehenry/azurefile:latest \
    AZURE_STORAGE_ACCOUNT=devstoreaccount1 \
    AZURE_STORAGE_ACCOUNT_KEY=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw== \
    AZURE_STORAGE_BASE=http://127.0.0.1:10000/


docker volume create -d jmaitrehenry/azurefile \
  -o account-name=devstoreaccount1 \
  -o account-key=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw== \
  -o storage-base=http://127.0.0.1:10000/ \
  --name myazurevolume

sudo apt install -y cifs-utils
docker plugin install --alias cloudstor:azure \
  --grant-all-permissions docker4x/cloudstor:17.06.1-ce-azure1  \
  CLOUD_PLATFORM=AZURE \
  AZURE_STORAGE_ACCOUNT_KEY="Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==" \
  AZURE_STORAGE_ACCOUNT="devstoreaccount1" \
  AZURE_STORAGE_ENDPOINT="http://localhost:10000/" \
  DEBUG=1


docker volume create -d azurefile --name myazurevolume -o share=myvol




docker plugin install --alias cloudstor:azure \
  --grant-all-permissions docker4x/cloudstor:17.06.1-ce-azure1  \
  CLOUD_PLATFORM=AZURE \
  AZURE_STORAGE_ACCOUNT_KEY="Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==" \
  AZURE_STORAGE_ACCOUNT="devstoreaccount1" \
  AZURE_STORAGE_ENDPOINT="http://localhost:10000/" \
  DEBUG=1




docker service create -p 3000:3000 --replicas 3 \
  --config my-config --config src=my-config,target=/config/my-config \
  --secret my-secret --secret src=my-secret,target=/secrets/my-secret \
  --name secret-and-config pietrom/node-secret-and-config:6

docker service create --name=viz --publish=8080:8080/tcp \
  --constraint=node.role==manager --mount=type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
  dockersamples/visualizer




docker manifest create \
    pietrom/dotnet-core-api-clock-api:2 \
    pietrom/dotnet-core-api-clock-api:2-lin \
    pietrom/dotnet-core-api-clock-api:2-win