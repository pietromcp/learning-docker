# Docker Swarm
172.19.11.100 --> bee0 (initial master)
172.19.11.101 --> bee1 (initially worker)
172.19.11.102 --> bee2 (initially worker)
172.19.11.103 --> bee3 (initially worker)
172.19.11.104 --> bee4 (initially worker)

172.19.11.200 --> honeycomb (file server)

# Docker, Docker Compose
172.19.17.5 --> harbor ()